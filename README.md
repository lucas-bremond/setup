# Lucas ▸ Dotfiles

Personal work setup.

![screenshot](./assets/screenshot-1.png)

## Operating System

[macOS 12 (Monterey)](https://www.apple.com/macos/monterey/)

## Applications

| Name                                                                   | Description                                                              |
| ---------------------------------------------------------------------- | ------------------------------------------------------------------------ |
| [Rectangle](https://rectangleapp.com/)                                 | Move and resize windows in macOS using keyboard shortcuts or snap areas. |
| [iStat Menus](https://bjango.com/mac/istatmenus/)                      | System monitoring widgets.                                               |
| [Little Snitch](https://www.obdev.at/products/littlesnitch/index.html) | Firewall.                                                                |

## Terminal

- [iTerm2](https://iterm2.com/)
- [Z shell](https://en.wikipedia.org/wiki/Z_shell)
- [Starship](https://starship.rs/)

iTerm 2 profile available [here](./iterm-2).

- Set iTerm 2 [margins](https://gitlab.com/gnachman/iterm2/issues/3571#note_56168105) to 20 / 20.
- [Remove the “Last login” Message from the Terminal](https://osxdaily.com/2010/06/22/remove-the-last-login-message-from-the-terminal/)

## Development Environment

- [Docker](https://www.docker.com/)
- [Docker Compose](https://docs.docker.com/compose/)
- [Visual Studio Code](https://code.visualstudio.com/)

### Utilities

| Name                                                      | Description                                                   |
| --------------------------------------------------------- | ------------------------------------------------------------- |
| [ctop](https://github.com/bcicen/ctop)                    | Top-like interface for container metrics.                     |
| [lazydocker](https://github.com/jesseduffield/lazydocker) | The lazier way to manage everything docker.                   |
| [The Fuck](https://github.com/nvbn/thefuck)               | Magnificent app which corrects your previous console command. |
| [bat](https://github.com/sharkdp/bat)                     | A `cat` clone with syntax highlighting and Git integration.   |
| [fzf](https://sourabhbajaj.com/mac-setup/iTerm/fzf.html)  | A general-purpose command-line fuzzy finder.                  |
| [tree](https://formulae.brew.sh/formula/tree)             | A recursive directory listing command.                        |
| [exa](https://the.exa.website/).                          | A modern replacement for `ls`.                                |
| [nnn](https://github.com/jarun/nnn)                       | A full-featured terminal file manager.                        |
| [zoxide](https://github.com/ajeetdsouza/zoxide)           | A smarter `cd` command.                                       |
| [xxh](https://github.com/xxh/xxh)                         | Bring shell through `ssh`.                                    |
| [dust](https://github.com/bootandy/dust)                  | A more intuitive version of `du`.                             |
| [duf](https://github.com/muesli/duf)                      | A better `df` alternative.                                    |
| [prettyping](https://github.com/denilsonsa/prettyping)    | A pretty wrapper around `ping`.                               |
| [procs](https://github.com/dalance/procs)                 | A modern replacement for `ps`.                                |
| [fd](https://github.com/sharkdp/fd)                       | A user-friendly version of `find`.                            |
| [ghq](https://github.com/x-motemen/ghq)                   | Manage remote repository clones.                              |
| [autojump](https://github.com/wting/autojump)             | A faster way to navigate your filesystem.                     |

### Visual Studio Code Extensions

| Name                                                                                                                   | Description                                                                                                                                      |
| ---------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------ |
| [Remote Development](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack) | An extension pack that lets you open any folder in a container, on a remote machine, or in WSL and take advantage of VS Code's full feature set. |
| [Git Graph](https://marketplace.visualstudio.com/items?itemName=mhutchie.git-graph)                                    | View a Git Graph of your repository, and perform Git actions from the graph.                                                                     |
| [Gitlab Workflow](https://marketplace.visualstudio.com/items?itemName=fatihacet.gitlab-workflow)                       | GitLab VSCode integration.                                                                                                                       |
| [Todo Tree](https://marketplace.visualstudio.com/items?itemName=Gruntfuggly.todo-tree)                                 | Show TODO, FIXME, etc. comment tags in a tree view.                                                                                              |
| [change-case](https://marketplace.visualstudio.com/items?itemName=wmaurer.change-case)                                 | Quickly change the case (camelCase, CONSTANT_CASE, snake_case, etc) of the current selection or current word.                                    |
| [DotENV](https://marketplace.visualstudio.com/items?itemName=mikestead.dotenv)                                         | Support for dotenv file syntax.                                                                                                                  |
| [GraphQL for VSCode](https://marketplace.visualstudio.com/items?itemName=kumar-harsh.graphql-for-vscode)               | GraphQL syntax highlighting, linting, auto-complete, and more!                                                                                   |
| [hexdump for VSCode](https://marketplace.visualstudio.com/items?itemName=slevesque.vscode-hexdump)                     | Display a specified file in hexadecimal.                                                                                                         |
| [VS Code Jupyter Notebook Previewer](https://marketplace.visualstudio.com/items?itemName=jithurjacob.nbpreviewer)      | An easy to use extension for previewing Jupyter Notebooks within VS Code.                                                                        |
| [vscode-input-sequence](https://marketplace.visualstudio.com/items?itemName=tomoki1207.vscode-input-sequence)          | Sequential numbers in VSCode.                                                                                                                    |

## Useful Utilities

### Docker

Utility scripts available [here](./docker).

Find and delete all images which name contains a given keyword:

```bash
docker-rmi "keyword"
```

Find and delete all "dirty" images (where the tag is not strictly following [semantic versioning](https://semver.org/), e.g., `0.3.0-97-g551e523`):

```bash
docker-rmi-dirty
```

Find and delete all "none" images:

```bash
docker-rmi-none
```

Find and delete all containers which name contains a given keyword:

```bash
docker-rm "keyword"
```
