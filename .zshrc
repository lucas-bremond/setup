# User configuration

# You may need to manually set your language environment
export LANG="en_US.UTF-8"

# Preferred editor for local and remote sessions
export EDITOR="nano"

export PATH="${HOME}/.local/bin:${HOME}/.local/bin/git-tools/commands:${PATH}"
export PATH="${PATH}:/usr/local/go/bin"

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

setopt HIST_IGNORE_SPACE

HISTFILE="${HOME}/.zsh_history"
HISTSIZE=999999999
SAVEHIST="${HISTSIZE}"

export CORRECT_IGNORE_FILE='.*'

export COLORTERM="truecolor"

export USE_GKE_GCLOUD_AUTH_PLUGIN="True"

export XDG_CONFIG_HOME="${HOME}/"
export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"

######################################################################################################################################################

# Shell

# https://stackoverflow.com/questions/60729605/word-forward-backward-delimiter-difference-between-bash-and-zsh
export WORDCHARS="${WORDCHARS/\//}"

## Starship

eval "$(starship init zsh)"

source "${HOME}/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh"

# Load Git completion
zstyle ':completion:*:*:git:*' script ${HOME}/.zsh/git-completion.bash
fpath=(${HOME}/.zsh $fpath)

autoload -Uz compinit && compinit

# Color completion for some things.
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}

# Select with arrow keys.
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
zstyle ':completion:*' list-colors ''
zstyle ':completion:*:*:*:*:*' menu select

# Faster completion.
zstyle ':completion::complete:*' use-cache 1

# Disable git info for mounted directories
# https://github.com/sindresorhus/pure/issues/189#issuecomment-173844254
zstyle ':vcs_info:*' disable-patterns "/mnt(|/*)"

# Flux completion
command -v flux >/dev/null && . <(flux completion zsh)

######################################################################################################################################################

# Utilities

## thefuck

if [ "$(command -v thefuck)" ]; then
    eval $(thefuck --alias)
fi

## ranger

if [ "$(command -v ranger)" ]; then
    alias rg='. ranger'
fi

## kubectl

if [ "$(command -v kubectl)" ]; then
    kc () {
        kubectl config get-contexts | tail -n +2 | fzf | cut -d" " -f2- | awk '{print $1}' | xargs kubectl config use-context
    }
fi

## zoxide

if [ "$(command -v zoxide)" ]; then
    eval "$(zoxide init zsh)"
fi

## lsd

if [ "$(command -v lsd)" ]; then
    unalias -m 'll'
    unalias -m 'l'
    unalias -m 'la'
    unalias -m 'ls'
    alias ls='lsd'
    alias ll='ls -l'
fi

## bat

if [ "$(command -v bat)" ]; then
  unalias -m 'cat'
  alias cat='bat -pp --theme="Nord"'
fi

## fzf

if [ "$(command -v fzf)" ]; then
    source <(fzf --zsh)
    [ -f ${HOME}/.fzf.zsh ] && source ${HOME}/.fzf.zsh
fi

## git

if [ "$(command -v git)" ]; then
    alias gs="git status"
    alias gc="git checkout"
    alias gr="git rebase"
    alias gcb="git checkout -b"
    alias gbd="git branch -D"
    alias grc="git rebase --continue"

    gcm () {
        git checkout $(git branch | cut -c 3- | grep -E '^master$|^main$') && git pull && git fetch -p
    }
fi

## tug

if [ "$(command -v tug)" ]; then
    source <(tug completion zsh)

    alias tcf="tug commit feat"
    alias tcc="tug commit chore"
    alias tcb="tug commit build"
    alias tct="tug commit test"
fi

## Docker

if [ "$(command -v docker)" ]; then
    alias di="docker images"
    alias dpsa="docker ps --all --format 'table {{.Names}}\t{{.Status}}\t{{.Image}}\t{{.Ports}}' | less -S"
    alias wdpsa="watch docker ps --all"
    alias dcp="yes | docker container prune"
    alias dvp="yes | docker volume prune --all"
    alias dnp="yes | docker network prune"
    alias docker-rm="${HOME}/.scripts/docker/docker-rm.sh"
    alias docker-prune="${HOME}/.scripts/docker/docker-prune.sh"
    alias docker-rmi="${HOME}/.scripts/docker/docker-rmi.sh"
    alias docker-rmi-none="${HOME}/.scripts/docker/docker-rmi-none.sh"
    alias docker-rmi-dirty="${HOME}/.scripts/docker/docker-rmi-dirty.sh"
    alias docker-clean="docker-rm || dvp & dnp"
    alias dlf="docker logs --follow"
fi

## Lazydocker

if [ "$(command -v lazydocker)" ]; then
    alias lzd="lazydocker"
fi

# https://github.com/derailed/k9s/issues/1017

export XDG_CONFIG_HOME=${HOME}

## GitLab

export GITLAB_API_TOKEN="$(cat ${HOME}/.credentials/gitlab-api-token)"
export GITLAB_COM_USER="lucas-bremond"
export GITLAB_COM_TOKEN="${GITLAB_API_TOKEN}"

export PACKAGE_DEPLOY_TOKEN="$(cat ${HOME}/.credentials/package-deploy-token)"
export PACKAGE_REGISTRY_URL="https://${PACKAGE_DEPLOY_TOKEN}:@pypi.fury.io/loft-orbital/"
export DEFAULT_FIREBASE_USER_EMAIL="lucas@loftorbital.com"
export COCKPIT_FIREBASE_USER_EMAIL=${DEFAULT_FIREBASE_USER_EMAIL}

alias gitlab="docker run -it --rm --env=GITLAB_API_TOKEN --entrypoint=python3 registry.gitlab.com/python-gitlab/python-gitlab:latest"

## Camunda

export CAMUNDA_CLUSTER_ID="69399560-4a7a-475a-b794-18042e5a7ee4"
export CAMUNDA_CLUSTER_REGION="dsm-1"

export CAMUNDA_CONSOLE_CLIENT_ID="EvOyHMRCo~7tvUZ4"
export CAMUNDA_CONSOLE_CLIENT_SECRET="$(cat ${HOME}/.credentials/camunda-console-client-secret)"

export CAMUNDA_OPERATE_CLIENT_ID="FRI-etxIEbcH.JrbFW.dpQ9HYWY..FEl"
export CAMUNDA_OPERATE_CLIENT_SECRET="$(cat ${HOME}/.credentials/camunda-operate-client-secret)"

## Misc

alias fix-auth="source ${HOME}/.scripts/fix_ssh_auth.sh && fix_ssh_auth"

## CUE

export CUEPRIVATE="gitlab.com/loft-orbital"

## Cesium

export CESIUM_TOKEN="$(cat ${HOME}/.credentials/cesium-token)"

######################################################################################################################################################

# Shortcuts

alias zshconfig="code ${HOME}/.zshrc"
alias ohmyzsh="code ${HOME}/.oh-my-zsh"
alias sshconfig="code ${HOME}/.ssh/config"

######################################################################################################################################################

# Google Cloud SDK

export CLOUDSDK_PYTHON_SITEPACKAGES=1

# The next line updates PATH for the Google Cloud SDK.
if [ -f "${HOME}/.local/google-cloud-sdk/path.zsh.inc" ]; then . "${HOME}/.local/google-cloud-sdk/path.zsh.inc"; fi

# The next line enables shell command completion for gcloud.
if [ -f "${HOME}/.local/google-cloud-sdk/completion.zsh.inc" ]; then . "${HOME}/.local/google-cloud-sdk/completion.zsh.inc"; fi

######################################################################################################################################################

# Loft Orbital

## General

export GOOGLE_APPLICATION_CREDENTIALS="${HOME}/.config/gcloud/application_default_credentials.json"

kube-enable-prod () {
    gcloud container clusters get-credentials main --zone us-central1-a --project loft-orbital-production
    kubectl config use-context gke_loft-orbital-engineering_us-west2-a_jupyterhub
}

kube-disable-prod () {
    kubectl config delete-context gke_loft-orbital-production_us-central1-a_main
    kubectl config use-context gke_loft-orbital-engineering_us-west2-a_jupyterhub
}

alias k9s-production="k9s --context=gke_loft-orbital-production_us-central1-a_main --namespace=cockpit --command=pods"
alias k9s-sandbox="k9s --context=gke_loft-orbital-sandbox_us-central1-a_main --command=pods"

## Engineering

alias dobby="k9s --context=gke_loft-orbital-engineering_us-central1-c_dobby --command=pods"

alias ccsds-space-packet="docker run -it --rm us.gcr.io/loft-orbital-engineering/libraries/python/ccsds/space-packet"
alias ccsds-cfdp="docker run -it --rm us.gcr.io/loft-orbital-engineering/libraries/python/ccsds/cfdp"
alias ecss-pus="docker run -it --rm us.gcr.io/loft-orbital-engineering/libraries/python/ecss/pus"

## Products

export AUTHENTICATION_DISABLED="false"
export AUTHORIZATION_DEFAULT_ROLE="SUPERUSER"

export FIREBASE_PROJECT_ID="loft-orbital-dev"
export FIREBASE_API_KEY="$(cat ${HOME}/.credentials/firebase-api-key)"

export DEFAULT_FIREBASE_USER_EMAIL="lucas@loftorbital.com"
export DEFAULT_API_TOKEN="loft-development-token"

export JWT_SIGNING_KEY="$(cat ${HOME}/.credentials/jwt-signing-key)"

### Cockpit

export COCKPIT_ROOT="${products}/cockpit"

#### Cockpit ▸ Production

export COCKPIT_API_URL_PRODUCTION="https://api.cockpit.cloud.loftorbital.com"
export COCKPIT_API_TOKEN_PRODUCTION="$(cat ${HOME}/.credentials/cockpit-api-token-production)"

alias k9s-production-cockpit="k9s --context=gke_loft-orbital-production_us-central1-a_main --namespace=cockpit --command=pods"

#### Cockpit ▸ Sandbox

export COCKPIT_API_URL_SANDBOX="https://api.cockpit.sandbox.loftorbital.com"
export COCKPIT_API_TOKEN_SANDBOX="$(cat ${HOME}/.credentials/cockpit-api-token-sandbox)"

alias k9s-sandbox-cockpit="k9s --context=gke_loft-orbital-sandbox_us-central1-a_main --namespace=cockpit --command=pods"

#### Cockpit ▸ Stage

export COCKPIT_API_URL_STAGE="https://api.cockpit.stage.loftorbital.com"
export COCKPIT_API_TOKEN_STAGE="$(cat ${HOME}/.credentials/cockpit-api-token-stage)"

alias k9s-stage-cockpit="k9s --context=gke_loft-orbital-cockpit-stage_us-central1-c_cockpit --namespace=cockpit --command=pods"

#### Cockpit ▸ Development

export COCKPIT_HOST="lucas-bremond.machine.dev.corp.loftorbital.com"

### Cutest

export CUTEST_ROOT="${products}/cutest"
export CUTEST_HOST="lucas-bremond.machine.dev.corp.loftorbital.com"

#### Cutest ▸ Production

export CUTEST_API_URL_PRODUCTION="https://api.cutest.loftorbital.com"
export CUTEST_API_TOKEN_PRODUCTION="$(cat ${HOME}/.credentials/cutest-api-token-production)"

#### Cutest ▸ Stage

# export CUTEST_API_TOKEN_STAGE="$(cat ${HOME}/.credentials/cutest-api-token-stage)"

#### Cutest ▸ Development

export CUTEST_API_TOKEN_DEVELOPMENT="${DEFAULT_API_TOKEN}"

### Oort

export OORT_ROOT="${products}/oort"
export OORT_HOST="lucas-bremond.machine.dev.corp.loftorbital.com"

#### Oort ▸ Production

export OORT_API_URL_PRODUCTION="https://api.oort.cloud.loftorbital.com"
export OORT_API_TOKEN_PRODUCTION="$(cat ${HOME}/.credentials/oort-api-token-production)"

alias k9s-production-oort="k9s --context=gke_loft-orbital-production_us-central1-a_main --namespace=oort --command=pods"

#### Oort ▸ Sandbox

export OORT_API_URL_SANDBOX="https://api.oort.sandbox.loftorbital.com"
export OORT_API_TOKEN_SANDBOX="$(cat ${HOME}/.credentials/oort-api-token-sandbox)"

alias k9s-sandbox-oort="k9s --context=gke_loft-orbital-sandbox_us-central1-a_main --namespace=oort --command=pods"

#### Oort ▸ Development

export OORT_API_TOKEN_DEVELOPMENT="${DEFAULT_API_TOKEN}"

#### Oort ▸ CLI

export OORT_CLI_VERSION="0.5.8"
export PATH="${HOME}/.oort/${OORT_CLI_VERSION}/bin:${PATH}"

alias oort-cli="docker run -it --rm --volume=$(pwd)/workspace:/workspace --user=root --env=OORT_API_TOKEN us.gcr.io/loft-orbital-oort/apps/oort-cli:${OORT_CLI_VERSION}"
